<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use Alert;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = Role::get();

        return view('admin.parameter.role.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $id_role =  $request->input('id_role');

        $count = Role::where('id_role', $id_role)->count();

        if($count > 0){

            return redirect('/role')->with(['warning' => 'Role Code already exist']);

        }        

        $data           =  new Role;
         
        $data->id_role     = $request->id_role;
        $data->role     = $request->role;
        $data->desc    =  $request->desc;
        $data->status =  '1';
        
        $data->save();

        return redirect('/role')->success('SuccessAlert','Data saved successfuly');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Role::where('id',$id)->first();

        return view('admin.parameter.role.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $role =  $request->input('role');
        $desc =  $request->input('desc');

        $data = Role::where('id',$id)->update(array('role' => $role, 'desc' => $desc ));

         return redirect('role')->with(['success' => 'Role data successfully updated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Role::where('id',$id)->delete();   
                        
        return redirect('role')->with(['success' => 'Role data successfully deleted']);
    }
}
