<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Authorizable;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = User::orderBy('id', 'DESC')->get();
        $role = Role::get();

        return view('admin.user.index', compact('data', 'role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email =  $request->input('email');
        $password =  $request->input('password');
        $pass = bcrypt('password');

        if($email > 0){

            return redirect('/user')->with(['warning' => 'Email  already exist']);

        } 

        $data           =  new User;
         
        $data->name     = $request->name;
        $data->email     = $request->email;
        $data->password     = $pass;
        $data->role_id    =  $request->role;
        $data->phone    =  $request->phone;
        $data->status =  '1';
        
        $data->save();


        return redirect('/user')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->first();
        $role = Role::get();

        return view('admin.user.edit', compact('data', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $name =  $request->input('name');
        $email =  $request->input('email');
        $password =  $request->input('password');
        $phone =  $request->input('phone');
        $role_id =  $request->input('role');

        $pass = bcrypt('password');


        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'password' => $pass, 'role_id' => $role_id, 'phone' => $phone));

         return redirect('user')->with(['success' => 'User data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
