<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Model\DetailResult;
use App\Model\HistorySearching;
use RealRashid\SweetAlert\Facades\Alert;



class SearchController extends Controller
{
    

    protected $soapWrapper;



    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.search.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$buyer_id = Auth::user()->id;

        $search = $request->input('search');


        $soap_Wrapper = new SoapWrapper();

        //$client = new \SoapClient($wsdl, $options);
                       
            $this->soapWrapper->add('cds', function ($service) {
                $service
                    ->wsdl('http://moaqs.com/GBLWS/GBLWS.asmx?WSDL');
            });

             
                $param=array(
                    'IDNo' => $search,
                    'txnCode' => '',
                    'ActCode' => 'R',
                    'usr' => '',
                    'pwd' => ''
                );
            
                $response = $this->soapWrapper->call('cds.GBLCallPndRmk',array($param));

                $smsMessages =  $response->GBLCallPndRmkResult
                ? $response->GBLCallPndRmkResult
                : array( $response->GBLCallPndRmkResult );

                
                        if($smsMessage->IDN == $search){
                            /*$hasil = DetailResult::where('ic', $pra->ic)->latest('updated_at')->where('updated_at','>=',$date1)->where('updated_at','<=',$date2)->whereIn('stage',['W13', 'W17'])
                                ->update(['pending_remark_moaqs' => $smsMessage->Rmk,'status_moaqs' => $smsMessage->TStat]);*/


                                $request                  = new DetailResult;

                                $request->vehicle_id_number          = $search;
                                /*$request->name            = $request->name;
                                $request->activity        = "1";
                                $request->remark_id       = "W0";
                                $request->stage_id        = "W0";
                                $request->user_id         = $user;
                                $request->note            = "";*/

                                $request->save();
                        }
                    

            

            return redirect('/search-vehicle')->with(['Success' => 'Vechile Number Found']);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.search.search_payment', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
