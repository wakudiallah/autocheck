<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Input;
use Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';


    protected function authenticated(Request $request, $user)
    {
        
        if (Auth::user()->role_id == 'US'){
             return redirect('/dashboard');
        }
        else if (Auth::user()->role_id == 'AD'){
            return redirect('/dashboard');
        }
        else if (Auth::user()->role_id == 'TEC'){
            return redirect('/administrator/teacher')->with(['Success' => 'Login Success']);
        }else{

            return redirect('/');
            //return redirect('/')->with(['warning' => 'Wrong Email/IC or Password']);
        }

        
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
        return view('auth.login2');
    }


    public function logout(Request $request) {
      Auth::logout();
      return redirect('/login2');
    }



    public function login_post()
    {
    // validate the info, create rules for the inputs
    $rules = array(
        'email'    => 'required|email', // make sure the email is an actual email
        'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
    );

    // run the validation rules on the inputs from the form
    $validator = Validator::make(Input::all(), $rules);

    // if the validator fails, redirect back to the form
    if ($validator->fails()) {
        return Redirect::to('login2')
            ->withErrors($validator) // send back all errors to the login form
            ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
    } else {

        // create our user data for the authentication
        $userdata = array(
            'email'     => Input::get('email'),
            'password'  => Input::get('password')
        );

        // attempt to do the login
        if (Auth::attempt($userdata)) {

            // validation successful!
            // redirect them to the secure section or whatever
            // return Redirect::to('secure');
            // for now we'll just echo success (even though echoing in a controller is bad)
            


            if (Auth::user()->role_id == 'US'){
             return redirect('/dashboard')->with('success', 'Login Successfully!');
            }
            else if (Auth::user()->role_id == 'AD'){
                return redirect('/dashboard')->with('success', 'Login Successfully!');
            }
            else if (Auth::user()->role_id == 'TEC'){
                return redirect('/administrator/teacher')->with(['Success' => 'Login Success']);
            }else{

                return redirect('/');
                //return redirect('/')->with(['warning' => 'Wrong Email/IC or Password']);
            }


        } else {        

            // validation not successful, send back to form 
            return Redirect::to('login2');

        }

    }
}
    
}
