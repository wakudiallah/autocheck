<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'WebController@index');

Route::get('/test', function () {
    return view('admin.dashboard');
});

Auth::routes();



//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login2', 'Auth\LoginController@login');
Route::post('/login2', 'Auth\LoginController@login_post');
Route::get('/authenticated', 'Auth\LoginController@authenticated');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/register-user', 'Auth\RegisterController@register_user');


Route::group( ['middleware' => ['auth']], function() {


/* Admin */
	
	Route::get('/home', 'DashboardController@index')->name('home');
	
	//--- Search 
	Route::get('/dashboard', 'DashboardController@index');
	Route::get('/search-vehicle', 'SearchController@index');
	Route::post('/search-vehicle', 'SearchController@store');
	Route::get('/search-payment', 'SearchController@show');



	Route::get('/vehicle_checked', 'SearchController@index');  //buat controller baru


	Route::get('user-profile', 'ProfileController@index');

	/* User */
	Route::get('user', 'UserController@index');
	Route::get('add-user', 'UserController@create');
	Route::post('save-user', 'UserController@store');
	Route::get('edit-user/{id}', 'UserController@edit');
	Route::post('update-user/{id}', 'UserController@update');


	/*Parameter*/
	Route::get('role', 'RoleController@index');
	Route::post('save-role', 'RoleController@store');
	Route::get('edit-role/{id}', 'RoleController@edit');
	Route::post('update-role/{id}', 'RoleController@update');
	Route::get('delete-role/{id}', 'RoleController@destroy');


	Route::get('brand', 'BrandController@index');
	Route::post('save-brand', 'BrandController@store');
	Route::get('edit-brand/{id}', 'BrandController@edit');
	Route::post('update-brand/{id}', 'BrandController@update');
	Route::get('delete-brand/{id}', 'BrandController@destroy');	

	/*Report*/
	Route::get('report-autocheck', 'ReportController@index');


});