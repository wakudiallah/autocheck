<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users'))
        {
            Schema::create('detail_results', function (Blueprint $table) {
                $table->increments('id');
                $table->string('vehicle_id_number');
                $table->date('search_date')->nullable();
                $table->tinyInteger('search_result')->nullable();
                $table->string('buyer_id', 200);
                $table->tinyInteger('payment_status')->default('0')->nullable();
                $table->string('search_from')->nullable();
                $table->boolean('status')->default('0')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_results');
    }
}
