@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">User</h3>
              </div>
            </div>
            <!-- End Page Header -->
            
            <div class="row">
              <div style="float: right;" class="mb-3">
               
                <a href="#" class="mb-2 btn btn-primary mr-2 btn-lg" data-toggle="modal" data-target="#exampleModal">Create New User</a>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      

                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>User Name</th>
                                  <th>Email</th>
                                  <th>Role</th>
                                  <th>Phone</th>
                                  <th>Status</th>
                                  <th>Last login time</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i = 1; ?>
                            @foreach($data as $data)
                              <tr>
                                  <td>{{$i++}}</td>
                                  <td>{{$data->name}}</td>
                                  <td>{{$data->email}}</td>
                                  <td>{{$data->role->role}}</td>
                                  <td>{{$data->phone}}</td>
                                  <td>
                                    @if($data->status = 1)
                                      Active
                                    @else
                                      Non Active
                                    @endif
                                  </td>
                                  <td>{{$data->updated_at}}</td>
                                  <td>
                                    <a href="{{url('edit-user/'.$data->id)}}">
                                      <button type="button" class="mb-2 btn btn-sm btn-primary mr-1"> 
                                        <i class="material-icons">create</i>
                                      </button>
                                    </a>
                                    <button type="button" class="mb-2 btn btn-sm btn-warning mr-1">
                                      <i class="material-icons">details</i>
                                    </button>
                                    <button type="button" class="mb-2 btn btn-sm btn-danger mr-1">
                                      <i class="material-icons">delete</i>
                                    </button>
                                  </td>
                                  
                              </tr>
                            @endforeach 
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>


            </div>
          </div>

          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-user')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" >
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone"> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Role</label>
                                <select class="form-control" name="role">
                                  <option value="" selected disabled hidden>- Select -</option>
                                  @foreach($role as $data)
                                  <option value="{{$data->id_role}}">{{$data->role}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>

          <!-- Modal -->

@endsection


@push('js')


  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

@endpush