<!DOCTYPE html>

<html>

<head>

	<title>Report</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<style type="text/css">
		body{ 
			font-family: calibri !important;
			font-size: 12px;
		}
	</style>

</head>

<body>
<!-- <img src="http://global.insko.my/admin/images/header.png" /> -->

	

	<table width="100%" border="1">
		<tr>
			<td width="30%" align="center">
				<br><br><br>
				<img src="{{asset('images/auto.jpg')}}" style="width:150px;height:70px; text-align:center"/>
				<h3>Autocheck Verification Report</h3>
			</td>
			<td width="20%">
				<h5 style="line-height: normal;">Verification Serial Number <br>00000784G</h5>
				<P></P>
			</td>
			<td width="50%">
				<img src="{{asset('images/MITI.png')}}" style="width:60px;height:70px;"/>
				<img src="{{asset('images/mai.jpg')}}" style="width:50px;height:30px;"/></td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td></td>
			<td align="center"><h3 style="font-size: 17px !important"><u>VERIFICATION REPORT</u></h3></td>
			<td></td>
		</tr>
	</table>


	<table width="100%">
		<tr>
			<td align="left">
				<table>
					<tr>
						<td>Requested By  </td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>NAZA GROUP</td>
					</tr>
					<tr>
						<td>Email Address</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>saimah@naza.com.my | 0142250680</td>
					</tr>
					<tr>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Verified By</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>Admin</td>
					</tr>
					<tr>
						<td>Verified Date</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>:</td>
						<td>24 May 2019</td>
					</tr>
				</table>
			</td>
			
			<td>
				<img src="{{asset('images/qrcode.png')}}" style="width:100px;height:120px;"/>
			</td>
		</tr>
	</table>



	<div class="row" style="margin-top: 50px !important; margin-bottom: 100px !important">
		<table class="table table-striped table-responsive"  width="100%"  	>
			<thead>
				<tr>
					<th>ID</th>
				    <th>Document</th>
				    <th>Check</th>
				</tr>
			</thead>

			<tbody>
			    <tr>
					<td>1</td>
					<td>IC</td>
					<td><input type="checkbox" checked> </td>
			    </tr>

			    <tr>
			    	<td>2</td>
			    	<td>Pay Slip</td>
			    	<!--<td>@if(empty($datax2->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	<td ><input type="checkbox" checked> </td>
			    </tr>

			    <tr>
			     	<td>3</td>
			     	<td>EA</td>
			     	<td ><input type="checkbox" checked> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td>4</td>
			     	<td>LOC</td>
			     	<td ><input type="checkbox" checked> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>
			    
			    <tr>
			     	<td>5</td>
			     	<td>Bank Account Detail</td>
			     	<td ><input type="checkbox" checked> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    <tr>
			     	<td>6</td>
			     	<td>App Form</td>
			     	<td ><input type="checkbox" checked> </td>
			     	<!-- <td>@if(empty($datax3->doc_pdf) ) X @else <input type="checkbox" checked> @endif</td> -->
			    	
			    </tr>

			    
			    
		    
		 	</tbody>
		</table>
	</div>



	<table width="100%">
		
		<tr>
			<td>By : </td>
		</tr>
	</table>


</body>
</html>