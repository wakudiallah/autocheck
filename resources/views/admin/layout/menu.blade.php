<div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="{{url('dashboard')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ Request::is('search-vehicle') ? 'active' : '' }}" href="{{url('search-vehicle')}}">
                  <i class="material-icons">search</i>
                  <span>Search Vehicle</span>
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle_checked') ? 'active' : '' }}" href="{{url('vehicle_checked')}}">
                  <i class="material-icons">check_box</i>
                  <span>Vehicle Checked</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="form-components.html">
                  <i class="material-icons">insert_chart</i>
                  <span>Vehicle Chart</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="tables.html">
                  <i class="material-icons">book</i>
                  <span>Invoice</span>
                </a>
              </li>


              <li class="nav-item">
                <a class="nav-link" href="#">
                  <i class="material-icons">settings</i>
                  <span>Setup Parameter</span>
                </a>
                <ul class="nav nav-second-level collapse">
                  <li><a href="{{url('role')}}">Role</a></li>
                  <li><a href="{{url('brand')}}">Brand</a></li>
                </ul>
              </li>


          <div class="nav-wrapper">
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link " href="index.html">
                  <i class="material-icons">edit</i>
                  <span>Blog Dashboard</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="components-blog-posts.html">
                  <i class="material-icons">vertical_split</i>
                  <span>Blog Posts</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="add-new-post.html">
                  <i class="material-icons">note_add</i>
                  <span>Add New Post</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="form-components.html">
                  <i class="material-icons">view_module</i>
                  <span>Forms &amp; Components</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="tables.html">
                  <i class="material-icons">table_chart</i>
                  <span>Tables</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="user-profile-lite.html">
                  <i class="material-icons">person</i>
                  <span>User Profile</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link " href="errors.html">
                  <i class="material-icons">error</i>
                  <span>Errors</span>
                </a>
              </li>
            </ul>
          </div>

              
              

             
              <li class="nav-item">
                <a class="nav-link {{ Request::is('logout') ? 'active' : '' }}" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="material-icons">&#xE879;</i>
                  <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </li>

               <li class="nav-item">
                <a class="nav-link {{ Request::is('report-autocheck') ? 'active' : '' }}" href="{{url('report-autocheck')}}" target="_blank">
                  <i class="material-icons">person_add</i>
                  <span>Report</span>
                </a>
              </li>

            </ul>
          </div>