<!--Header_section-->
    <header id="header_outer">
        <div class="container">
            <div class="header_section">
                <div class="logo"><a href="javascript:void(0)"><img src="{{asset('web/img/logobx.png')}}" alt=""></a></div>
                <nav class="nav" id="nav">
                    <ul class="toggle">
                        <li><a href="#top_content">Home</a></li>
                        <li><a href="#service">Services</a></li>
                        <li><a href="#work_outer">Work</a></li>
                        <li><a href="#Portfolio">Portfolio</a></li>
                        <li><a href="#client_outer">Clients</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#contact">Contact</a></li>
                        @guest
                        <li><a href="{{url('login2')}}">Login</a></li>
                        @else
                        <li><a href="{{url('dashboard')}}">Admin</a></li>
            
                        @endguest
                    </ul>
                    <ul class="">
                        <li><a href="#top_content">Home</a></li>
                        <li><a href="#service">Services</a></li>
                        <li><a href="#work_outer">Work</a></li>
                        <li><a href="#Portfolio">Portfolio</a></li>
                        <li><a href="#client_outer">Clients</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#contact">Contact</a></li>
                        @guest
                        <li><a href="{{url('login2')}}">Login</a></li>
                        @else
                        <li><a href="{{url('dashboard')}}">Admin</a></li>
            
                        @endguest
                    </ul>
                </nav>
                <a class="res-nav_click animated wobble wow" href="javascript:void(0)"><i class="fa-bars"></i></a> </div>
        </div>
    </header>
    <!--Header_section-->